### Description

This is a simple http server with a single endpoint to serve as a `TermUrl` target in the mastercard pay and reload topup process.

It exposes a single http POST endpoint at `/topupcomplete` and returns a html page which can be crawled for a `PaRes`.

### Setup

1. Install dependencies

```
$ npm install
```

2. Start webserver

```
$ npm start
```