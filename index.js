const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const env = require('./env')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded())
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Credentials', true)
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET,POST')
  next()
})

app.post('/topupcomplete', (req, res) => {
    const paRes = req.body && req.body.PaRes
    const webViewContent = `
        <html>
            <head>
                <title>PaRes:${paRes}</title>
            </head>
            <body>
                <div style="width: 100%; height: 100%; display: flex; flex: 1; align-items: center; justify-content: center;">
                    <svg width="200px"  height="200px"  xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-flickr" style="background: none;">
                        <circle ng-attr-cx="{{config.cx1}}" cy="50" ng-attr-fill="{{config.c1}}" ng-attr-r="{{config.radius}}" cx="59.2473" fill="#ec1111" r="20">
                        <animate attributeName="cx" calcMode="linear" values="30;70;30" keyTimes="0;0.5;1" dur="3" begin="-1.5s" repeatCount="indefinite"></animate>
                        </circle>
                        <circle ng-attr-cx="{{config.cx2}}" cy="50" ng-attr-fill="{{config.c2}}" ng-attr-r="{{config.radius}}" cx="40.7527" fill="#fdc823" r="20">
                        <animate attributeName="cx" calcMode="linear" values="30;70;30" keyTimes="0;0.5;1" dur="3" begin="0s" repeatCount="indefinite"></animate>
                        </circle>
                        <circle ng-attr-cx="{{config.cx1}}" cy="50" ng-attr-fill="{{config.c1}}" ng-attr-r="{{config.radius}}" cx="59.2473" fill="#ec1111" r="20">
                        <animate attributeName="cx" calcMode="linear" values="30;70;30" keyTimes="0;0.5;1" dur="3" begin="-1.5s" repeatCount="indefinite"></animate>
                        <animate attributeName="fill-opacity" values="0;0;1;1" calcMode="discrete" keyTimes="0;0.499;0.5;1" ng-attr-dur="{{config.speed}}s" repeatCount="indefinite" dur="3s"></animate>
                        </circle>
                    </svg>
                </div>
                <input type="hidden" id="PaRes" name="PaRes" value="${paRes}">
            </body>
        </html>
    `
  res.send(webViewContent)
})

const httpHandler = http.Server(app)
httpHandler.listen(env.port, function () {
  console.log(`listening on *:${env.port}`)
})